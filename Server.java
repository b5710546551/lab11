package lab11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

public class Server extends AbstractServer{
	private Map<String,ConnectionToClient> clients = new HashMap<String,ConnectionToClient>();
	private int state;
	final int LOGGEDIN=0;
	final int LOGGEDOUT = 2;
	final int SENDING = 4;
	private ConnectionToClient toClient;
	public Server(int port) {
		super(port);
	}
	protected synchronized void clientConnected(ConnectionToClient client){
		client.setInfo("state", LOGGEDOUT);
	}
	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message = (String)msg;
		if(!(msg instanceof String)){
			sendToClient(client,"Unrecognized Message");
			return ;
		}

		state = (Integer) client.getInfo("state");
		switch(state){
		case LOGGEDOUT:

			if(message.matches("Login \\w+")){
				String name = message.substring(6).trim();
				if(!clients.containsKey(name))
					clients.put(name,client);
				client.setInfo("username",name);
				client.setInfo("state", LOGGEDIN);
				sendToAllClients(name + " logged in");
			}
			else {
				sendToClient(client,"Please Login");
				return;
			}
			break;

		case LOGGEDIN: 
			if(message.equalsIgnoreCase("logout")){
				sendToClient(client,"Goodbye");
				String name = (String)client.getInfo("username");
				sendToAllClients(name + " logged off");
				client.setInfo("state", LOGGEDOUT);
				clientDisconnected(client);
			}
			else if(message.startsWith("To: ")){
				String name = message.substring(3).trim();
				if(clients.containsKey(name)){
					toClient = clients.get(name);
					sendToClient(client, "Please type your message: ");
					client.setInfo("state", SENDING);
				}
			}
			else{
				super.sendToAllClients(client.getInfo("username") + " : " + message);
			}
			break;
		
		case SENDING:
			sendToClient(toClient, message + " from "+ client.getInfo("username"));
			sendToClient(client,"Message to " + toClient.getInfo("username")+" : " + message);
			client.setInfo("state", LOGGEDIN);
			break;
		}

	}

	private void sendToClient(ConnectionToClient client, Object msg) {
		try {
			client.sendToClient(msg);
		} catch (IOException e) {
		}

	}

	
	protected synchronized void clientDisconnected(ConnectionToClient client){
		clients.remove(client);
	}
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		Server server = new Server(5555);
		System.out.println("Starting Server");
		try {
			server.listen();
		} catch (IOException e) {
			System.out.println("Fail to connect to Server");
		}

	}

}
