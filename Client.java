package lab11;

import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;

public class Client extends AbstractClient{

	public Client(String host, int port) throws IOException {
		super(host, port);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);
	}


	public static void main(String[] args) throws IOException{	
		Scanner in = new Scanner(System.in);
		Client client = new Client("158.108.181.241",5001);
		client.openConnection();
		while(client.isConnected()){
			System.out.println("Msg to send: ");
			String msg = in.nextLine().trim();
			if(msg.equalsIgnoreCase("quit")){
				client.closeConnection();
				break;
			}
			else client.sendToServer(msg);
			client.handleMessageFromServer(msg);
		}

	}
}
